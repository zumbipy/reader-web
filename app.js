const path = require('path');
const express = require('express');

const app = express();
const port = 3000;

// Configuração de Views
app.set('views', path.join(path.resolve('src', 'views')));
app.set('view engine', 'ejs');

app.use(express.static('src'))
app.get('/', (req, res) => res.render('index'))

app.listen(port, () => {
    console.log(`Sevirdor Fucionado: http://localhost:${port}/`)
})